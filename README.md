臻云极致-远程SDK与相关的测试工具

与一般的我们其它一般的SDK不同的是，我们目前的臻云极致SDK支持通过远程SN设备序列号访问设备。

- `IEControlTalkDemo.zip` IE浏览器远程观看视频和语音功能
- `vzcloud_java_sdk_and_demo1.1.zip` JAVA远程连接的Demo和SDK
- `车牌识别一体机开发包1.2.1.18.zip` 完整的一体机远程开发包
- `VZFireFox语音对讲插件1.01` 在FireFox下面远程播放视频和语音的插件。目前只支持FireFox49版本及其以下的版本。
- `tools\OEM工具1.0.0.15.zip` 是带有远程功能的OEM工具
- `tools\一体机配置工具1.2.0.5.zip` 是带有远程功能的一体机配置工具
- `vzcloud_web_demo.rar` 网页版Demo，与官方网站基本一样，用户可以将这个挂到服务器上面，自己建立云端。只需要稍微进行定制化修改就行。
